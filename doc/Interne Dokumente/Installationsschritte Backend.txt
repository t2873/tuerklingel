##############################################
#Dinge die später noch am Code geändert werden müssen: 

- IP-Adresse von Uni-Raspberry PI muss in ESP32-Programm durch IP des privaten Raspberry-PI´s ersetzt werden




#############################################
VM-Einstellungen: 

- Um sich per VM mit Handy-Hotspot zu verbinden, muss man Bridged einstellen und bei "Replicate physical network connection state" einen Haken machen. 
  Der Host-PC muss dabei mit dem Hotspot verbunden sein 










################################################
#ZUSAMMENFASSUNG DER ZU INSTALLIERENDEN PAKETE: 
#

+ steht für: Command wurde bereits auf dem Hochschule Raspberry PI ausgeführt 
(R) steht für: Command wurde bereits auf meinem privaten Raspberry PI ausgeführt 
("R") steht für: Command wurde bereits auf privatem Raspberry PI ausgeführt aber Command wurde zu diesem Dokument leicht abgeändert 

#>sudo apt upgrade python3   (R) 
#>sudo apt install python3-pip    +  (R)
#>pip3 install flask    +   (Error angeblich nicht in PATH installiert in /home/pi/.local/bin)   (R)
#>pip3 install flask-socketio    +  (R)
#>pip3 install flask_mqtt (R) 
#>pip3 install pymysql 	   +   (R)
#>pip3 install pandas    +    (Error angeblich nicht in PATH installiert in /home/pi/.local/bin)  (R)
#>pip3 install-upgrade numpy (falls es Probleme mit pandas gibt) (R)
#sudo apt-get install libatlas-base-dev (falls es Probleme mit Numpy gibt -> Rapsberry) (R)
#>pip3 install eventlet (R)
#>pip3 install gevent (R)
#>pip3 install pygame  ("R")
#//Kann nicht mit pip Paketinstaller installiert werden 
#sudo apt-get install python3-opencv (R)
#sudo apt install git       +     (R)
#
#//Holt sich nötiges Repository für MySQL Installation 
#>wget http://repo.mysql.com/mysql-apt-config_0.8.20-1_all.deb
#>sudo apt install ./mysql-apt-config_0.8.20-1_all.deb
#>sudo apt install default-mysql-server
#>sudo mysql_secure_installation
#
#AUF DEM RASPBERRY PY habe ich MariaDB laufen 
#sudo apt-get install mariadb-server -y    (R)
#sudo mysql_secure_installation    (R)
#sudo mysql -uroot    (R)
################################################

Mosqitto installieren auf Raspberry:
sudo apt install mosquitto (R)
sudo apt install mosquitto-clients (R)    //Dadurch bekommt man zum Testen noch die Befehle des Clients "pub" und "sub"
anschließend die .conf Datei von /usr/share/doc/mosquitto/examples mit >sudo mv mosquitto.conf /etc/mosquitto/conf.d kopieren
Datei öffnen und bei listener das "#" entfernen und den Port 1883 hinterlegen 
Bei Security weiter unten das '#' bei allow_anonymous entfernen und true dahinert schreiben. So kann man sich ohne Authentifizierung mit dem MQTT-Broker verbinden 
!Hier muss man später aufpassen, weil man in Kombination mit Websocket vielleicht Probleme bekommt (http://www.steves-internet-guide.com/mossquitto-conf-file/)
Jetzt Mosquitto-Dienst erneut starten: >sudo systemctl restart mosquitto 
(Es ist wichtig, dass der Dienst einmal komplett runtergefahren ist, weil sonst die Änderungen nicht wirksam werden) 




MySQL Datenbank anbinden: 


Bei Linux extra Schritt: 
>sudo apt install python3-pip 	  //Zuerst muss man dort pip installieren  



Installiere Socket: 
>pip install flask-socketio




WIR VERWENDEN JETZT (>pip3 pymysl)

>pip install flask-mysqldb        //Braucht man um mit flask die DB anzusteuern 




	Unter Windows bei MYSQL anmelden (MySQL Shell):
	>\sqll 
	>\connect root@localhost:3306
	>Password eingeben 


	Passwort für Benutzer ändern: 
	>alter user 'root'@'localhost' identified by 'NEUES PASSWORT'

Datenbank erstellen: 

>create database flaskapp;    	//hinten ist Name der Datenbank 
>use flaskapp; 			//wechselcht zur Datenbank flaskapp		
	

Tabelle erstellen: 

>create table log(id INT AUTO_INCREMENT, uhrzeit VARCHAR(20), datum VARCHAR(20), systemzustand VARCHAR(20), tuerkontakt VARCHAR(20), tueroeffner VARCHAR(20), klingeltaster VARCHAR(20), oeffnungstaster VARCHAR(20), wegbeleuchtung VARCHAR(20), PRIMARY KEY (id)); 
Weil es sonst zu Fehlern kommen kann, füllen wir die Tabelle  mit mind. 5 Einträgen 
>INSERT INTO log(uhrzeit, datum, systemzustand, tuerkontakt, tueroeffner, klingeltaster, oeffnungstaster, wegbeleuchtung) VALUES ('test', 'test', 'test', 'test', 'test', 'test', 'test', 'test');
>create table konfiguration (id INT AUTO_INCREMENT, systemzustand VARCHAR(20), begruessungston VARCHAR(20), PRIMARY KEY (id));
>INSERT INTO konfiguration (systemzustand, begruessungston) VALUES ('test', 'test');
>create table emails (id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT "Primary Key', email VARCHAR(255) COMMENT ''); 


>create table users(name varchar(20), email varchar(40)); 


Um Password zu ändern keine Datenbank auswählen beim Starten von MariaDB: 
>SET PASSWORD FOR 'root'@localhost = PASSWORD ("123456");

###################################

import smtplib
from email.message import EmailMessage


E-Mail und SMS Push Nachrichten installieren 

Alle nötigen Bibliotheken sind bereits bei Python standardisiert enthalten 

Man muss jedoch eine G-Mail Adresse erstellen, welche dann als E-Mail Sender verwendet wird 

EMAIL: tuerklingel13@gmail.com
PASSWORT: LLoPxxy323!
APP-Passwort: ztlzvanuikfiysse

GMail muss noch konfiguriert werden. Schritte werden ab 6:30 bei https://www.youtube.com/watch?v=B1IsCbXp0uE erklärt

#####################################

Alternative E-Mail (FLASK): 

pip3 install Flask-Mail

Ansonsten selbe Konfiguration des GMail Accounts wie oben bereits beschrieben 


###################################
ESP32

Arduino IDE konfigurieren: 
- Datei > Voreinstellungen, dort: https://dl.espressif.com/dl/package_esp32_index.json bei "zustätzliche Boardverwalter URL's" eingeben 
- Im Boardverwalter nach esp32 suchen und Paket installieren 
- Bei Borad ESP32 Dev Modul auswählen 


MQTT konfigurieren: 
- PubSubClient Bibliothek herunterladen von: https://github.com/knolleary/pubsubclient -> Downloadlink: https://github.com/knolleary/pubsubclient/archive/master.zip
- In ArduinoIDE einbinden 

ButtonEvent konfigurieren:
- Bibliothek von https://github.com/fasteddy516/ButtonEvents herunterladen. 
- Ordner in Arduino "libraries" einfügen 
- Sketch -> Bibliothek einbinden 

Dasselbe muss man noch mit https://github.com/thomasfredericks/Bounce2 machen. Button Event ist von dieser Bibliothek abhängig 
