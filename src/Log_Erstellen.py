# Mit pandas kann man Dataframes erstellen, welche eine tabellerische Textformatierung besitzen 
import pandas as pd
from pathlib import Path
import csv



from Singleton import Singleton
class LogErstellen(metaclass = Singleton):

    def __init__(self): 
       
        print("Log wird erstellt")

    # Erstellt eine Log-Datei, welche später vom Webserver zum Download angeboten wird 
    def Datei_Erstellen(self, datenbank_log):       
        
        # Wandelt das Tupel datenbank_log, welches alle Logeinträge aus Datenbank enthält in eine Liste um
        datenliste = []
        
       
        for zeile in datenbank_log:
            datenliste.append(list(zeile))
            


        # Erweiterung "pandas" macht aus Liste eine strukturierte Tabelle 
        # Columns sind die Titel der einzelnen Tabellenspalten
        daten_rahmen = pd.DataFrame(datenliste, columns=['id','uhrzeit','datum','systemzustand','tuerkontakt','tueroeffner','klingeltaster','oeffnungstaster','wegbeleuchtung'])
        
        # Tabelle wird in eine csv Datei gespeichert
        # Header = true > Zeige Spaltennamen, index = True > Nummerierung, sep = \t > Separation der Elemente, mode = w > Überschreibe alte Datei
        datei = 'log.csv'
        datei_pfad = Path('static/log/')
        datei_pfad.mkdir(parents=True, exist_ok=True)
        
        daten_rahmen.to_csv(datei_pfad/datei, header=True, index=False, sep=',', mode='w+')
        
        
         

        

       
    

    

       
        

      
       
        

