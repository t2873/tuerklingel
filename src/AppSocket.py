
from flask import Flask
from flask_socketio import SocketIO
from Singleton import Singleton
from flask_mqtt import Mqtt

# Klasse erstellt Objekte app und socket, welche Webserver und Socket-Verbindung konfigurieren 
# Enthält Methoden, welche den Webserver starten und Nachrichten an Front-End schicken
class AppSocket(metaclass = Singleton):


    def __init__(self): 

        # Erstellen des App Objektes von Flask 
        self.app = Flask(__name__)
        
        # Hier werden dinge wie MQTT-server Ip/port configuriert
        self.app.config['TEMPLATES_AUTO_RELOAD'] = True
        self.app.config['SECRET'] = 'my secret key'
        # Server ip unterschiedlich je nach Router
        self.app.config["MQTT_BROKER_URL"] = "192.168.178.160" # Jakobip: 192.168.178.59 Lenardsip: 192.168.43.182 (Hotspot) 192.168.178.161 (WLAN)
        self.app.config["MQTT_BROKER_PORT"] = 1883
        self.app.config['MQTT_USERNAME'] = "FlaskApp"
        self.app.config['MQTT_CLIENT_ID'] = 'FlaskAppID'
        self.app.config['MQTT_KEEPALIVE'] = 5
        
        
        # Erstellen eines Socket Objektes. Als Parameter wird das eben erstelle App-Objekt übergeben
        self.socketio = SocketIO(self.app)
        self.mqtt = Mqtt(self.app)
        
 
    # Konfiguriert das App-Objekt so, dass Server bei eingehenden URL Anfragen bestimmte Funktionen aufruft 
    def AppSocketKonfigurieren(self):
        
        import Funktionen
        
        # Die Konfiguration wurde aus der Hauptdatei "backend.py" ausgelagert. 
        # Dekorator @app.route('') wurde durch direkten Funktionsaufruf add_url_rule ersetzt
        
        # Später alles über socket auser download und Startseite
        
        self.app.add_url_rule('/downloadlog','downloadlog',Funktionen.downloadlog)

        self.app.add_url_rule('/','/',Funktionen.startseite)

        #self.app.add_url_rule('/öffnen','/öffnen',Funktionen.oeffnen)

        # Url muss entfernt werden und dient nur Testzwecken.
        # Wird später automatisch durch Klingelevent ausgeführt 
        self.app.add_url_rule('/bild','/bild',Funktionen.BildMachen)

        # Url-Adressiertung der Audiodateien zum Abspielen der Begrüßungsnachrichten     
        self.app.add_url_rule('/static/begruesungaufnahmen/<path:filename>', endpoint='aufnahmen',view_func=self.app.send_static_file)
        
        
        
        
    
        ########################################################################
        # URL Routen werden für die simulierten Eingänge erstellt 

        # URL Roule für simulieren Klingeltaster (MUSS SPÄTER WIEDER ENTFERNT WERDEN)
        self.socketio.on_event('klingeltaster',Funktionen.klingeln)

        #self.app.add_url_rule('/kontakt','/kontakt',Funktionen.kontakt)

        #self.app.add_url_rule('/oeffnungstaster','/oeffnungstaster',Funktionen.oeffnungstaster)

        #self.app.add_url_rule('/licht','/licht',Funktionen.licht)

        #########################################################################

        # Konfigutiert den Socket so, dass bei eingehender Nachricht mit Tag "nachricht" eine Funktion ausgeführt wird
        self.socketio.on_event('Nachricht',Funktionen.VerarbeiteNachricht)
        
        # Konfigutiert den Socket so, dass bei eingehender Nachricht mit Tag "emaillöschen" die Funktion EmailEmpfängerLöschen ausgeführt wird
        self.socketio.on_event("EmailLöschen", Funktionen.EmailEmpfängerLöschen)
        
        self.socketio.on_event("EmailHinzufügen", Funktionen.EmailEmpfängerHinzufügen)
        
        self.socketio.on_event("WebsiteTuerOeffner", Funktionen.WebsiteTuerOeffner)
        
        
        
        # Wenn der Server gestartet wird und der Esp32 noch an ist/sich im verlauf noch verbindet sprechen sie sich über die zustände ab
        self.mqtt.subscribe("ESP32Verbunden")
        
        
        # Hier werden die Topics festgelegt auf welche der Mqtt client achten soll
        self.mqtt.subscribe("Klingeltaster")
        self.mqtt.subscribe("Lichtstärke")
        self.mqtt.subscribe("Türkontakt")
        self.mqtt.subscribe("Wegbeleuchtung")
        self.mqtt.subscribe("TueroeffnerAntwort")
        self.mqtt.subscribe("Oeffnungstaster")
        # Hier wird gesagt, welche Funktion ausgeführt werden soll wenn eine Bestimmtes Topic angesprochen wird
        
        
        # Hacky Workaround, dass die eigentliche Funktion in einer Anderen Datei abgespeichert ist
         # Wir wollten nicht das @ In funktionen stehen haben und alles in eine Andere Datei auslagern, das wist einziger weg
        @self.mqtt.on_topic('Klingeltaster')
        def mockKlingeltaster(client, userdata, message):
            print("okokokokokok")
            Funktionen.Klingeltaster(client, userdata, message)
        
        @self.mqtt.on_topic("Lichtstärke")
        def mockLichtstärke(client, userdata, message):
            Funktionen.Lichtstärke(client, userdata, message)
            
        @self.mqtt.on_topic("Türkontakt")
        def mockTürkontakt(client, userdata, message):
            Funktionen.Türkontakt(client, userdata, message)
            
        @self.mqtt.on_topic("Wegbeleuchtung")
        def mockTürkontakt(client, userdata, message):
            Funktionen.Wegbeleuchtung(client, userdata, message)
        
        # ESP schickt als Bestätigung, dass Tueroeffner 5sek öffnet eine Nachricht 
        @self.mqtt.on_topic("TueroeffnerAntwort")
        def mockTürkontakt(client, userdata, message):
            Funktionen.TuerOeffner(client, userdata, message)
        
        @self.mqtt.on_topic("Oeffnungstaster")
        def mockTürkontakt(client, userdata, message):
            Funktionen.Oeffnungstaster(client, userdata, message)
            
        @self.mqtt.on_topic("ESP32Verbunden")
        def mockTürkontakt(client, userdata, message):
            Funktionen.ESP32Verbunden(client, userdata, message)
        
        self.NachrichtanEsp32("ServerStart")
            
        
        
        
        
        
        
        
        
        
    # Methode mit welcher Nachrichten per Socket an script.js von allen verbundene Benutzer gesendet werden 
    def NachrichtAnBenutzer(self, daten, event='WebsiteAktualisieren'):
        self.socketio.emit(event, daten, broadcast = True)
        print('Nachricht an alle verbundene Benutzer gesendet')

        # Methode startet den Socket und übergibt dazu das app-Objekt 
    def StarteWebserver(self):
        self.socketio.run(self.app, port=5000, host='0.0.0.0',debug=True)

    def NachrichtanEsp32(self,topic,Nachricht=""):
        self.mqtt.publish(topic,Nachricht)