# Hier sind die Funktionen der anderen Klassen ausgelagert und strukturiert worden.


from textwrap import indent
import eventlet
from flask import render_template
from flask import send_from_directory
from flask import Response
from AppSocket import AppSocket

# Muss gemacht werden, dass flask_socketio mit flask_mqtt funktioniert
eventlet.monkey_patch()

#########################################################
####Import von eigenen Klassen###########################
import Datenstruktur
import Email_Dienst
import Kamera
import DB_Zugriff
import Log_Erstellen
import AppSocket
import AudioSpieler
import ThingSpeak
import GPIOPins

#######################################################

# Instanzierung aller benötigten Klassen 

datenstruktur = Datenstruktur.Datenstruktur()

kamera = Kamera.Kamera()

db_zugriff = DB_Zugriff.DB_Zugriff()

# Empfänger der E-Mails müssen bereits bei der Instanzierung übergeben werden 
email_dienst = Email_Dienst.Email_Dienst()

log_erstellen = Log_Erstellen.LogErstellen()

app_socket = AppSocket.AppSocket()

audio_spieler = AudioSpieler.AudioSpieler()

thingspeak = ThingSpeak.ThingSpeakVerbindung() 

#gpiopins = GPIOPins.GPIOPins()

#################################################################################################################

# Ab hier werden die einzelnen Funktionen definiert und deren Anwendungen beschrieben


# Hauptfunktion die den Gesamten Server Startet!
def ServerStart():

    # Die einzelnen URLs werden festgelegt und die Websocket Nachrichten vorbereitet
    app_socket.AppSocketKonfigurieren()

    # Holt die akuellen Werte 
    NeueWerteHolenSystemstart()
    
    
    
    # Server wird gestartet
    app_socket.StarteWebserver()
    
    
    
##########################################################################################################
#### Das sind die Funktionen die über URLs aufgerufen werden



# Webserver gibt dem Client die HTML-Startseite zurück, welche im Browser angezeigt wird 
def startseite():
    # Als Parameter wird das Wörterbuch "Konfiguration" übergeben, um die Zustände der "index.htmk" bei Aufruf zu aktualisieren 
    return render_template('index.html',**datenstruktur.Konfiguration)

# Webserver bietet dem Client den Downloadlog zur Verfügung an 
def downloadlog():
    

    # Datenbankzugriff auf SQL-Tabelle log wird durchgeführt.
    # Methode Datei_Erstellen macht aus Tupel eine strukturierte Tabelle und schreibt diese in Datei 
    log_data = db_zugriff.LogAusLesen()
    log_erstellen.Datei_Erstellen(log_data)
 
    
    print("Log wird runtergeladen")
    
    # As Attachement signalisiert dem Browser, dass die Datei heruntergeladen werden soll
    # Greift direkt auf src Verzeichnis zu. Das Relative Arbeitsverzeichnis ist deswegen einfach nur /log
    return send_from_directory(path='log.csv', directory = 'static/log', as_attachment=True)
    



# Öffnet die Tür (Simuliert zumindest eine geöffnete Tür)
"""def oeffnen():
   
    return render_template('page.html')"""

######################################################################################################
# Hier werden Werte wie die GPIO pins und Systemzustand gelesen

# Holt die aktuellen Werte von Datenbank und GPIO Pins
def NeueWerteHolenSystemstart():
    systemzustand_begruessungston, fünfbesucher=db_zugriff.KonfigurationLesen()
    emailjson = db_zugriff.EmailsAuslesen()
    
    
    # Aktualisiert internes Wörterbuch 
    datenstruktur.AktualisiereDaten(systemzustand_begruessungston)

    datenstruktur.AktualisiereDaten(fünfbesucher)
    datenstruktur.AktualisiereDaten(emailjson)
    datenstruktur.AktualisiereDaten(kamera.LetzerBesucherBase64())
    
    # Hier muss noch das Base64 Bild von dem letzten bild in die Konfiguration geladen werden
    # Auch überlegen ob Email und Base64 auch über datenstruktur.AktualisiereDaten() Passieren soll... oder alles über eigene Funktion
    
    email_dienst.EmailEmpfängerAktualisieren(datenstruktur.Konfiguration["emails"])


    
############################################################################################################
# Funktion neue Zeiten/Zustände werden für den Log bereitgestellt und Log geschrieben

def EintragLog():


    db_zugriff.LogEintrag(datenstruktur.LogEintragWerte())


    # Holt sich nach dem Log die neuen Werte, damit der Besucher jetzt auch in der aktuellen Datenstruktur eingetragen wird 
    systemzustand_begruessungston,fuenf_besucher = db_zugriff.KonfigurationLesen()
    # Aktualisiert internes Konfigurations-Wörterbuch 
    datenstruktur.AktualisiereDaten(fuenf_besucher)
    
    
##########################################################################################################################

def EmailEmpfängerLöschen(Email):
    
    print("EmailLöschen Nachricht")
    
    datenstruktur.EmailEmpfängerLöschen(Email)
    
    db_zugriff.EmailEmpfängerLöschen(Email)
    
    app_socket.NachrichtAnBenutzer(Email, "EmailLöschen")
    
    # Alle Emails mit der neuen werden für das Versenden der Email beim Klingeln festgelegt
    email_dienst.EmailEmpfängerAktualisieren(datenstruktur.Konfiguration["emails"])
    
    
    

def EmailEmpfängerHinzufügen(Email):
    
    print("EmailHinzufügen Nachricht")
    
    datenstruktur.EmailEmpfängerHinzufügen(Email)
    
    db_zugriff.EmailEmpfängerHinzufügen(Email)
    
    app_socket.NachrichtAnBenutzer(Email, "EmailHinzufügen")

    # Alle Emails mit der neuen werden für das Versenden der Email beim Klingeln festgelegt
    email_dienst.EmailEmpfängerAktualisieren(datenstruktur.Konfiguration["emails"])
   
    
    
    
############################################################################################################################
# Email werden an Empfänger geschickt

def EmailSchicken():
    print("Email/-s geschickt.")
    email_dienst.EmailSchicken(db_zugriff.LetzterLogEintrag())


#############################################################################################################################
# Funktion die Später ein Bild macht über die Kameraklasse. Im moment nur Simuliert da keine Kamera an der VM hängt


# Macht Bild und zeigt es auf Weboberfläche an 
def BildMachen():
    
    # Methode BildAufnhemen erstellt ein Bild und speichert es lokal ab. Bild kann dann von Front-End angezeigt werden 
    Bild_als_Base64 = kamera.BildAufnehmen()
    datenstruktur.AktualisiereDaten(Bild_als_Base64)
    
    


###################################################################################################
# Zwei Funktionen um die MP3 dateien ausgeben zu lassen

def KlingelTonAbspielen():
    
    audio_spieler.SpieleKlingelTon()
    
def BegruessungsAnsageAbspielen():
    audio_spieler.SpieleBegruessungsansage(datenstruktur.Konfiguration["begruessungston"])



#########################################################################################################################
# Nachricht vom Client wird empfangen und Verarbeitet. 
# Danach wird an alle anderen Clients der neue Systemzustand/Begrüssungsauswahl informiert.



# Empfängt Daten aus Front-End von einem bestimmten Client.
# Schickt Socket-Nachrichten an restliche Clients mit veränderten Daten
# Speichert Daten im internen Konfigurations-Wörterbuch ab  
def VerarbeiteNachricht(daten):

    # Aktualisiert die Daten im internen Verzeichnis
    
    datenstruktur.AktualisiereDaten(daten)
    


    # Aktualisiert die Konfigurationsdatenbank mit den neusten Werten
    db_zugriff.KofigurationEintrag(datenstruktur.LogEintragWerte())
    

    # Sendet die Nachricht an den SocketListener im Javascript und von dort aus ins Front-End 
    
    app_socket.NachrichtAnBenutzer(datenstruktur.Konfiguration, "WebsiteAktualisieren")
    


def WebsiteTuerOeffner():
    
    # Hier noch nachricht an den ESP
    app_socket.NachrichtanEsp32("WebsiteTuerOeffner")
    
    app_socket.NachrichtAnBenutzer("","TuerOeffnerGedrueckt")
    
    
    

################## Funktionen für MQTT Nachrichten ###############################
# Hier wird gesagt, welche Funktion ausgeführt werden soll wenn eine Bestimmtes Topic angesprochen wird




# MQTT Nachricht muss von Byte zu UTF-8 umgewandelt werden 

def WandleInUTF8(message):
    print(message.topic)
    message_utf8 = message.payload.decode("utf-8")
    print(message_utf8)
    return message_utf8



def Lichtstärke(client, userdata, message):
    Lux = WandleInUTF8(message)
    
    app_socket.NachrichtAnBenutzer(Lux,"Lichtstärke")

# Initiale Funktion für wenn der Esp32 und der Server sich erstmalig verbinden
def ESP32Verbunden(client, userdata, message):
    temp = {
        "gpio_zustaende" : {
            "tuerkontakt":"leer", #High wenn Tür geschlossen
            "tueroeffner":"high", #High wenn Tür verriegelt ist  
            "klingeltaster":"leer", #High wenn jemand klingelt
            "oeffnungstaster":"low", #High wenn jemand den physischen Tür öffner drückt
            "wegbeleuchtung":"low" #High wenn der Gehweg beleuchtet ist
        }  
    }
    # Wenn der ESP32 verbunden ist, müssen die zustände so sein wie sie hier gesetzt werden
    zustände = WandleInUTF8(message).split(",")
    temp["gpio_zustaende"]["tuerkontakt"]=zustände[0]
    temp["gpio_zustaende"]["klingeltaster"]=zustände[1]
    
    
       
    datenstruktur.AktualisiereDaten(temp)
    app_socket.NachrichtAnBenutzer(datenstruktur.Konfiguration)
    
    



def Klingeltaster(client, userdata, message):
    
    empfangeneNachricht = WandleInUTF8(message)
    
    if(empfangeneNachricht == "Klingeltaster gedrueckt"):
    
        datenstruktur.Konfiguration["gpio_zustaende"]["klingeltaster"] = "high"
        
        # In klingeln wird die ganze logik von Logeintrag usw. gehandhabt
        klingeln()
    
    elif(empfangeneNachricht == "Klingeltaster losgelassen"):
        
        datenstruktur.Konfiguration["gpio_zustaende"]["klingeltaster"] = "low"
    
    # Sendet aktuelle GPIO-Zustaende an Front-End
    app_socket.NachrichtAnBenutzer(datenstruktur.Konfiguration)
    
    # Aktuelle thingspeak zustände schicken
    thingspeak.Aktualisiere_Werte(datenstruktur.Konfiguration["gpio_zustaende"])
    


def Türkontakt(client, userdata, message):
    
    empfangeneNachricht = WandleInUTF8(message)
    
    if(empfangeneNachricht == "Tuer geschlossen"):
        datenstruktur.Konfiguration["gpio_zustaende"]["tuerkontakt"] = "high"
    elif(empfangeneNachricht == "Tuer offen"):
        datenstruktur.Konfiguration["gpio_zustaende"]["tuerkontakt"] = "low"
        
    app_socket.NachrichtAnBenutzer(datenstruktur.Konfiguration)
    # Aktuelle thingspeak zustände schicken
    thingspeak.Aktualisiere_Werte(datenstruktur.Konfiguration["gpio_zustaende"])

def Wegbeleuchtung(client, userdata, message):
    
    empfangeneNachricht = WandleInUTF8(message)
    
    if(empfangeneNachricht == "Licht an"):
        datenstruktur.Konfiguration["gpio_zustaende"]["wegbeleuchtung"] = "high"
    elif(empfangeneNachricht == "Licht aus"):
        datenstruktur.Konfiguration["gpio_zustaende"]["wegbeleuchtung"] = "low"
        
    
    app_socket.NachrichtAnBenutzer(datenstruktur.Konfiguration)
    # Aktuelle thingspeak zustände schicken
    thingspeak.Aktualisiere_Werte(datenstruktur.Konfiguration["gpio_zustaende"])

# Methode für Oeffnungstaster
def Oeffnungstaster(client, userdata, message):
    
    empfangeneNachricht = WandleInUTF8(message)

    if(empfangeneNachricht == "Oeffnungstaster gedrueckt"):
        datenstruktur.Konfiguration["gpio_zustaende"]["tueroeffner"] = "high"
    elif(empfangeneNachricht == "Oeffnungstaster losgelassen"):
        datenstruktur.Konfiguration["gpio_zustaende"]["tueroeffner"] = "low"

    app_socket.NachrichtAnBenutzer(datenstruktur.Konfiguration)
    # Aktuelle thingspeak zustände schicken
    thingspeak.Aktualisiere_Werte(datenstruktur.Konfiguration["gpio_zustaende"])



#Methode zur Verriegelung
def TuerOeffner(client, userdata, message):
    
    empfangeneNachricht = WandleInUTF8(message)
    print("Tueroeffner Antwort erhalten")
    
    if(empfangeneNachricht == "Tueroeffner an"):
        datenstruktur.Konfiguration["gpio_zustaende"]["tueroeffner"] = "high"
    elif(empfangeneNachricht == "Tueroeffner aus"):
        datenstruktur.Konfiguration["gpio_zustaende"]["tueroeffner"] = "low"
        
    app_socket.NachrichtAnBenutzer(datenstruktur.Konfiguration)
    # Aktuelle thingspeak zustände schicken
    thingspeak.Aktualisiere_Werte(datenstruktur.Konfiguration["gpio_zustaende"])



# Diese Methoden senden per MQTT Befehl an ESP zum Tuer oeffnen 
# Während ESP Tür öffnet wird Bestätigungsnachricht gesendet. Siehe "TuerOeffner"    
def TuerOeffnenSteigendeFlanke(gpiopin):

    print("Steigende Flanke")
    # Nachricht zum Oeffnen der Tür wird an ESP32 geschickt
    app_socket.NachrichtanEsp32("Tueroeffner", "tuer_auf")
    
    # Wenn der Reale TuerOeffner gedrückt wird, erscheint ein popup auf der Weboberfläche
    app_socket.NachrichtAnBenutzer("","TuerOeffnerGedrueckt")
    # Lokaler zustand des Öffnungstasters im haus wird auf low gesetzt
    datenstruktur.Konfiguration["gpio_zustaende"]["oeffnungstaster"] = "high"
     # Alle benutzer und thingspeak werden über die neuen zustände in kenntnis gebracht
    app_socket.NachrichtAnBenutzer(datenstruktur.Konfiguration)
    # Aktuelle thingspeak zustände schicken
    thingspeak.Aktualisiere_Werte(datenstruktur.Konfiguration["gpio_zustaende"])

    
def TuerOeffnenFallendeFlanke(gpiopin):

    print("Fallende Flanke")
    # Lokaler zustand des Öffnungstasters im haus wird auf low gesetzt
    datenstruktur.Konfiguration["gpio_zustaende"]["oeffnungstaster"] = "low"
    # Alle benutzer und thingspeak werden über die neuen zustände in kenntnis gebracht
    app_socket.NachrichtAnBenutzer(datenstruktur.Konfiguration)
    # Aktuelle thingspeak zustände schicken
    thingspeak.Aktualisiere_Werte(datenstruktur.Konfiguration["gpio_zustaende"])




###############Funktionen für die Logik##################

# Funktion entscheidet wie System regiert wenn Klingeltaster gedrückt wird 
# Als Parameter wird Konfigurations-Wörterbuch übergeben mit aktuellem Systemzustand
def klingeln():

    systemzustand = datenstruktur.Konfiguration["systemzustand"]

    #Wenn Systemzustand aus:    
    if(systemzustand=="aus"):
        

        print("Zustand: aus")
        BildMachen()
        EintragLog()

        # Nachricht an alle Clients um aktuelle Daten ans Front-End zu liefern
        app_socket.NachrichtAnBenutzer(datenstruktur.Konfiguration)
        

    elif(systemzustand =="manuell"):

        print("Zustand: manuell")

        KlingelTonAbspielen()
        EintragLog()

        BildMachen()
        EmailSchicken()
        

        app_socket.NachrichtAnBenutzer(datenstruktur.Konfiguration)


    elif(systemzustand == "automatisch"):

        print("Zustand: automatisch")

        BegruessungsAnsageAbspielen()
        EintragLog()

        BildMachen()
        EmailSchicken()
        
        
        app_socket.NachrichtAnBenutzer(datenstruktur.Konfiguration)
        

    else:

        print("Fehler Systemzustand nicht erkannt. Klingeln wurde nicht ausgelöst.")













    
    
    
