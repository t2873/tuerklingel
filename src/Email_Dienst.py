# Wird benötigt, um mittels eines SMTP-Servers Emails zu verschicken 
from fileinput import filename
import smtplib

# Wird benötigt, um Email Nachrichten Objekte zu erzeugen 
from email.message import EmailMessage

from Singleton import Singleton




# Hat die Aufgabe Emails zu verschicken
# Dazu müssen zum einen eine Empfängerliste und zum anderen der Inhalt der Email übergeben werden 
class Email_Dienst(metaclass = Singleton): 


    

    def __init__(self):
        
        # Email-Adresse von welcher die Push-Nachricht gesendet wird 
        self.email = 'tuerklingel13@gmail.com'
        # Passwort um sich bei Google Mail anzumelden
        self.passwort = 'ztlzvanuikfiysse'

        # Erzeugt ein Objekt der Klasse EmailMessage
        self.msg = EmailMessage()
        
        


    # Verschickt eine Email
    def EmailSchicken(self,daten): 

        
        # Inhalt muss gelöscht werden, um evtl. bestehenden Inhalt zu löschen
        self.msg.clear_content()
        
        # Legt den Inhalt der Email fest. 
        # Übergebener Parameter muss vom Typ String sein. Kommt in diesem Fall von Klasse DB_Zugriff
        print(daten)
        self.msg.set_content(daten)
        
        
        # Hier wird das aktuelle Bild geöffnet
        with open("static/pics/ueberwachungsbild.jpg", "rb") as besucher_bild:
            
            # Hier wird das bild in Bytes gelesen
            bild_bytes = besucher_bild.read()
            
            # Und das Bild an die Email angehängt mit den Namen "Besucher"
            self.msg.add_attachment(bild_bytes, maintype='image', subtype='jpg', filename='Besucher.jpg')
        
        
          
       
        # Startet einen server, welcher E-Mails über das SMTP Protokoll verschickt  
        # In unserem Fall verwenden wir den SMTP Email Server von Google 
        smtp_server = smtplib.SMTP("smtp.gmail.com", 587)
        
        # Leitet eine verschlüsselte Übertragung ein 
        smtp_server.starttls()
        
        # Logt sicht beim Gmail-SMTP Server ein
        smtp_server.login(self.email, self.passwort)

        # Sendet die Email mit der vorher getroffenen Konfiguration 
        smtp_server.send_message(self.msg)

        #Nachdem E-Mail versendet wurde, wird Server wieder geschlossen
        smtp_server.quit()
        
    def EmailEmpfängerAktualisieren(self, EmailArray):
        
        self.msg.clear()
        
        # Legt Betreff fest
        self.msg['subject'] = 'Türklingel betätigt'
        
        # Legt fest wer der Absender der Email ist 
        self.msg['from'] = self.email
        
        # Legt Empfänger der Email fest
        # Empfängerliste ist langer String getrennt durch Kommas mit meheren Adressanten 
        
        tempEmailString = ""
        
        # 
        for Email in EmailArray:
            tempEmailString += Email + ","
            
        # Letzter Charakter ist ein , -> muss entfernt werden
        tempEmailString = tempEmailString[:-1]
            
        self.msg['to'] = tempEmailString