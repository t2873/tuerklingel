// Hier werden die Variablen und Konstanten initialisiert die in der Script.js gebraucht werden
const zustandContaioner = document.getElementById("Zustand");
const TachoZiel = document.getElementById('Tachometer');
var Tachometer;
const LuxAnzeige = document.getElementById('LuxAnzeige');

// Hier muss die Addresse vom IP eigetragen werden
var socket = io();



// Alle Funktionen die mit dem Löschen einer E-Mail zu tun haben

        // Nachricht an alle Clients auf der Website, die spezifische Email aus dem Dropdown zu löschen
        socket.on("EmailLöschen", (Email) => {
            console.log("Es kam eine Nachricht auf Emaillöschen an mit der Email: " + Email)
            EmailLöschen(Email);
        });
        
        // Das ist die Funktion die ausgeführt wird bei Drücken des Delete knopfes neben den Emails
        EmailLöschenKnopf = (Email) =>{
            console.log("Löschen Knopf gedrückt für " + Email)
            socket.emit("EmailLöschen",Email)
        }

        // Das ist die eigentliche Funtkion für Löschen der Email
        EmailLöschen = (Email) =>{
            var row = document.getElementById(Email);
            // Dadurch das der Serven JEDEN auf der Website Benachrichtigt, die Email zu Löschen, wird auch der Client benachrichtigt, von dem die Anfrage ausging
            // Deswegen wird row bei diesem Client Undefined sein und das wird hier abgefangen
            if (row != undefined){
                console.log(Email + " wird gelöscht.")
                row.parentNode.removeChild(row);
            }

            EmailFeldFarbeSetzen(Email);
        }




// Alle Funktionen die mit dem erstellen einer neuen E-Mail in der Tabelle und der syntaktischen Überprüfung zu tun haben. 

        // Verhindert das Automatische einklappen bei drücken der Deleteknöpfe in der Emailliste
        document.getElementById("Emailliste").addEventListener("click", function(e) {
            e.stopPropagation();
        });



        // Wird der "Hinzufügen Knopf" gedrückt wird der Inhalt des Input Feldes durch die Funktion "Validiere E-Mail" geprüft und bei Korrektheit an den Server geschickt.
        // Von dort aus geht sie an jeden Benutzer der Website und wird der Tabelle hinzugefügt.
        EmailHinzufügenKnopf = (Email) =>
        {

            var inputfeldEmail = document.getElementById("EmailInputFeld")
            const Valid = inputfeldEmail.reportValidity()

            // Hier wird die E-Mail Überprüft. Ist sie korrekt, so wird an den Socket eine Nachricht mit dem Inhalt der eingegebenen Mail geschickt. 
            if(ValidiereEmail(Email) && (document.getElementById(Email) == undefined) && Valid){
                console.log("Speichern Knopf gedrückt für " + Email)
                socket.emit("EmailHinzufügen",Email)
            }
            else{
                console.log(Email + " ist keine Valide Emailaddresse oder ist schon eigetragen.")
            }
        }


            // Funktion die den korrekten Syntax der E-Mail Addresse überprüft. Eingabeformat muss "@"" und gültige Endung enthalten und darf keine Anführungszeichen haben.
            const ValidiereEmail = (Email) => {
                // Vorlage zur Syntaxprüfung REGEX von https://stackoverflow.com/questions/46155/whats-the-best-way-to-validate-an-email-address-in-javascript
                return Email.match(
                    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                );
            };


        // Erhält der Socket die Nachricht eine E-Mail hinzufügen, so wird die Funktion EmailHinzufügen aufgerufen. Diese ruft zwei weitere Unterfunktionen auf.
        socket.on("EmailHinzufügen", (Email) => {
            console.log("Es kam eine Nachricht auf EmailHinzufügen an mit der Email: " + Email)
            EmailHinzufügen(Email);
        });

        // Die beiden Unterfunktionen fügen dynamisch eine Reihe in die Tabelle der E-Mail Empfänger ein und setzen das Eingabefeld auf eine Farbe.
        // (rot bei falscher Mail und grün bei richtiger Mail)
        EmailHinzufügen = (Email) =>{

            console.log(Email + " wird hinzugefügt.")

            EmailZuTabelleHinzufügen(Email)
            
            EmailFeldFarbeSetzen(Email)
            
        }


        // Das hier ist die Funktion welche die Farben des Eingabefeldes anpasst.
        // Wenn die eingegebene E-Mail dem Format entspricht (grün) oder wenn sie dem nicht entspricht (rot). 
        EmailFeldFarbeSetzen = (Email) => {

            Eingabefeld = document.getElementById('EmailInputFeld');

            if(Eingabefeld.value == ""){
                Eingabefeld.style.color = "black";
                return;
            }

            if(!(document.getElementById(Email) == undefined)){
                Eingabefeld.style.color = "lightcoral";
                return;
            }

            if(!ValidiereEmail(Email)){
                Eingabefeld.style.color = "lightcoral";
                return;
            }

            Eingabefeld.style.color = "lightgreen";
        }

        // Das hier ist die Funktion die automatisch die eingegebene E-Mail in der Oberfläche in die Tabelle schreibt
        // Dabei wird dynamisch eine neue Reihe in der Tabelle angelegt, welche als Inhalt die E-Mail Adresse, sowie den Löschen Button für diese enthält.
        EmailZuTabelleHinzufügen = (Email) =>{

            var Tabele = document.getElementById("EmailTabelle");
            var LöschenKnopf = document.createElement('button');
            var Reihe = Tabele.insertRow(0);
            var LöschenKnopfIcon = document.createElement('icon')

            Reihe.id = Email

            // Hier wird die Reihe in die Tabelle dynamisch eingefügt.
            var Zelle1 = Reihe.insertCell(0);
            Zelle1.style.position = "relative";
            Zelle1.style.float = "left";
            Zelle1.textContent = Email;

            var Zelle2 = Reihe.insertCell(1);
            Zelle2.style.position = "relative";
            Zelle2.style.float = "right";
            Zelle2.appendChild(LöschenKnopf);

            // Das hier ist der Löschen Knopf
            LöschenKnopf.type = "button";
            LöschenKnopf.addEventListener('click', function(){
                EmailLöschenKnopf(Email);
            });
            LöschenKnopf.appendChild(LöschenKnopfIcon);

            LöschenKnopfIcon.className = "bi-trash-fill"
            
        }





// Vom Server meldet alle aktuellen Informationen dem Client
socket.on("WebsiteAktualisieren", (daten) => {
    WebseiteAktualisieren(daten);
});

// Hier werden die aktuellen Informationen auf der Oberfläche dargestellt
WebseiteAktualisieren = (daten) => {
    
    console.log("Website wird Aktuallisiert...");
    console.log(daten);

    //Hier wird der Systemzustand anhand der Empfangenen Daten geändert
    if (document.getElementById("systemzustandaende") != null){
        document.getElementById("systemzustandaende").value=daten["systemzustand"];
    }


    //Hier wird der Begruessungston anhand der Empfangenen Daten geändert
    if (document.getElementById("begruessungston") != null){
        document.getElementById(daten["begruessungston"]).checked = true;
    }

    //Hier werden die letzten 5 Besucher anhand der Empfangenen Daten geändert
    for (let [besucher,werte] of Object.entries(daten["letzte_5_besucher"])){
        document.getElementById(besucher).textContent = "Am " + werte["datum"] + " wurde um " + werte["uhrzeit"] + " Uhr geklingelt"
    };
    

    // Hier wird bei einem neuen Bild das Bild Akutualisiert, da es als Base64 geschickt wird
    
    document.getElementById('Besucher')
    .setAttribute(
        'src',
        'data:image/jpg;base64,' + daten["base64_bild"]
    );
    




    //Hier werden die Zustände der Pins anhand der Empfangenen Daten aktuallisiert
    
    for (let [schluessel, wert] of Object.entries(daten["gpio_zustaende"])){
        if (wert == "leer") {
            document.getElementById(schluessel).classList.remove("kreis-grün");
            document.getElementById(schluessel).classList.remove("kreis-rot");
            document.getElementById(schluessel).classList.add("kreis-grau");
        }
        else if (wert == "low") {
            document.getElementById(schluessel).classList.add("kreis-rot");
            document.getElementById(schluessel).classList.remove("kreis-grün");
            document.getElementById(schluessel).classList.remove("kreis-grau");
        }
        else if (wert == "high") {
            document.getElementById(schluessel).classList.remove("kreis-grau");
            document.getElementById(schluessel).classList.add("kreis-grün");
            document.getElementById(schluessel).classList.remove("kreis-rot");
        };

        
    };
    

    



};


// Ab hier werden die 3 Nachrichten an den Server definiert
SendeAenderung = (nachricht) => {
    console.log("Update an Server")
    console.log(nachricht)
    socket.emit("Nachricht", nachricht);
};

SystemzustandAenderung = (zustandsauswahl) => {
    var neuer_zustand = {
        "systemzustand":zustandsauswahl.value,
    };

    SendeAenderung(neuer_zustand);

    
};

BegruesungsAenderung = (begruesung) =>{
    
    neuer_begruessungston = {
        "begruessungston":begruesung.id,
    };
    SendeAenderung(neuer_begruessungston);
};






// dieser Bereich definiert die Parameter/Konfiguration der Tachometer Anzeige
// Ebenfalls wird hiermit die Tachometer Anzeige überhaupt erstellt

// Tachoanzeige: https://bernii.github.io/Tachometer.js/


// Das sind die beiden Funktionen die die Lichtstärke vom ESP 32 erhalten und dynamisch als Wert in die Tachoanzeige/Luxfeld reinschreiben.

LichtstärkeAktualisieren = (Lux) => {
    Lux = parseFloat(Lux)
    Tachometer.set(Lux)
    LuxAnzeige.textContent = Lux + " Lux"
}

socket.on("Lichtstärke", (Lux) => {
    LichtstärkeAktualisieren(Lux);
});

// Hier wird die Tachoanzeige erstellt
TachoInit = () =>{
    // Hier werden die optischen Einstellungen des Tachometers vorgegeben
    var opts = {
        angle: 0, // The span of the Tachometer arc
        lineWidth: 0.2, // The line thickness
        radiusScale: 1, // Relative radius
        pointer: {
          length: 0.55, // // Relative to Tachometer radius
          strokeWidth: 0.042, // The thickness
          color: '#000000' // Fill color
        },
        limitMax: false,     // If false, max value increases automatically if value > maxValue
        limitMin: false,     // If true, the min value of the Tachometer will be fixed
        colorStart: '#000000',   // Colors
        colorStop: '#FFFFFF',    // just experiment with them
        strokeColor: '#AED4FF',  // to see which ones work best for you
        generateGradient: true,
        highDpiSupport: true,     // High resolution support
        // renderTicks is Optional
        renderTicks: {
          divisions: 5,
          divWidth: 1.1,
          divLength: 0.7,
          divColor: '#333333',
          subDivisions: 3,
          subLength: 0.5,
          subWidth: 0.9,
          subColor: '#666666'
        },

        // Hier wird die Tachoanzeige je nach Ausschlag in verschiedene Farbbereiche unterteilt
        percentColors : [[0.0, "#212529" ], [0.2, "#CFC568"], [1.0, "#FFA500"]],
        
        // Hier werden die Tachoanzeige gelabelt 
        staticLabels: {
            font: "10px sans-serif",  // Specifies font
            labels: [0, 200,700, 1200, 1700, 2200, 2700, 3200, 3700],  // Print labels at these values
            color: "#FFFFFF",  // Optional: Label text color
            fractionDigits: 0  // Optional: Numerical precision. 0=round off.
          },
      };

    // Hier wird die Tachoanzeige erstellt. Dabei wird auf die ID "Tachometer" in der HTML referiert. 
    console.log(TachoZiel)
    Tachometer = new Gauge(TachoZiel).setOptions(opts); // create sexy Tachometer!
    Tachometer.maxValue = 3774; // set max Tachometer value
    Tachometer.setMinValue(0);  // Prefer setter over Tachometer.minValue = 0
    Tachometer.animationSpeed = 50; // set animation speed (32 is default value)
    Tachometer.set(0) 
}






// Wenn der Benutzer einen der 2 Knöpfe auf der Oberfläche drückt wird ein Nachricht an den Server geschickt, dass die Tür entriegelt werden soll
TuerOeffner = () => {
    socket.emit("WebsiteTuerOeffner")
}


//  Jederz Benutzer bekommt nach dem ein Benutzer den Knopf auf der Oberfläche gedrückt hat ein Popup
socket.on("TuerOeffnerGedrueckt", () => {
    setTimeout(function() { alert('Türöffner wurde gedrückt! Die Tür ist jetzt für 5 Sekunden offen.'); }, 1);
});

















// Hier wird das Datum in der Navigationsbar dynamisch eingeschrieben
        // Aktualisierung des Datums erfolgt alle 5 Minuten = 300 Sekunden = 300000 Millisekunden
        const clockContainer = document.getElementById("clock");

        // Hier wird das aktuelle Datum in einen String geschrieben
        refreshClockContainer = () => {
            clockContainer.textContent = new Date().toDateString()
            console.log("clock");
        };

        setInterval(()=> {
            refreshClockContainer()
        },300000);


        // Hier wird festgelegt, dass das die Datumsfunktion von Anfang an geladen wird.
        init = () => {
            refreshClockContainer();
            EmailFeldFarbeSetzen(document.getElementById("EmailInputFeld").value);
            TachoInit();
        }

        init();













//// !!!!!!!!!!!!! WERDEN NICHTMEHR VERWENDET


// Hier wird eine Nachricht an den Server gesendet, dass die Türklingel gedrückt werden soll


// Mock funtkion für den Website Klingeltaster

KlingelTaster = () => {
    
    socket.emit("klingeltaster");
    console.log("Klingeltaster gedrückt");
    
}




TuerKontakt = () => {
    console.log(document.getElementById("tuerkontakt").classList)
    console.log("Tuersensor wurde verändert");

    document.getElementById("tuerkontakt").classList.add("kreis-rot");
    document.getElementById("tuerkontakt").classList.remove("kreis-grün");
    setTimeout(function(){
        document.getElementById("tuerkontakt").classList.add("kreis-grün");
        document.getElementById("tuerkontakt").classList.remove("kreis-rot");
    }, 3000);

};

OeffnungsTaster = () => {
    document.getElementById("oeffnungstaster").classList.add("kreis-grün");
    document.getElementById("oeffnungstaster").classList.remove("kreis-rot");
    setTimeout(function(){
        document.getElementById("oeffnungstaster").classList.add("kreis-rot");
        document.getElementById("oeffnungstaster").classList.remove("kreis-grün");
    }, 2000);
}

Wegbeleuchtung = () => {
    document.getElementById("wegbeleuchtung").classList.add("kreis-grün");
    document.getElementById("wegbeleuchtung").classList.remove("kreis-rot");
    setTimeout(function(){
        document.getElementById("wegbeleuchtung").classList.remove("kreis-grün");
        document.getElementById("wegbeleuchtung").classList.add("kreis-rot");
    }, 5000);
}