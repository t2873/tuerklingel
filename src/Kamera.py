#Importiert Bibliothek für Kamerazugriff und Bildbearbeitung 
import cv2
# Wird benötigt um Bild in das Base64 Format umzuwandeln 
import base64
import os.path

from Singleton import Singleton

# Macht Bild, speichert es lokal ab und returned es, damit es direkt per Websocket übertragen werden kann 
class Kamera(metaclass = Singleton): 

    def __init__(self):
        
        print("Objekt Kamera erstellt")
        
    # Nimmt ein Bild auf und speichert es in der lokalen Ordnerstruktur 
    def BildAufnehmen(self): 

        # Öffnet die Standardkamera. Die 0 steht für die 1. Kamera 
        cap = cv2.VideoCapture(cv2.CAP_V4L2)
        
        if cap is None or not cap.isOpened():
            print("Es konnte kein Bild aufgenommen werden. Entweder ist keine Kamera angeschlossen oder sie ist durch ein anderes Programm in Benutzung!")
            return {}
        
        print("Bild gemacht!")

        # Mit dieser Einstellung kann man die Dimension des Bildes einstellen
        """cap.set(3, 600)
        cap.set(4, 600)
        dim=(600,600)"""

        # Bild wird aufgenommen 
        ret, frame = cap.read()
        
        ret, buffer = cv2.imencode(".jpg",frame)

        # Bild wird unter folgendem Pfad gespeichert
        cv2.imwrite("static/pics/ueberwachungsbild.jpg", frame)

        # Speichert Bild in Base64 String ab
        

        # Schaltet die Kamera wieder aus
        cap.release()
        
        
        
        
        b64_string = base64.b64encode(buffer)
        
        
            

        # Gibt Bild zurück um es später über Websocket zu schicken
        # Zudem wird Bild als Base64 zurückgegeben, um es in Datenstruktur zu speichern
        return {"base64_bild":b64_string.decode('utf-8')}
    
    def LetzerBesucherBase64(self):
        print("lese letztes Bild")
        if os.path.isfile("static/pics/ueberwachungsbild.jpg"):
            print("Bild gelesen")
            Bild = cv2.imread("static/pics/ueberwachungsbild.jpg")
            ret, buffer = cv2.imencode(".jpg",Bild)
            return {"base64_bild":base64.b64encode(buffer).decode('utf-8')}
        else:
            return {}

