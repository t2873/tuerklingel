# tuerklingel von Lenard Haas, Manuel Moritz, Jakob Hock
from Funktionen import ServerStart
import socket

# Um den Webserver zu starten braucht man nur die Funktion "ServerStart"

if __name__ == "__main__":
    
    hostname = socket.gethostname()
    ip_addresse = socket.gethostbyname(hostname)
    
    print("################################################################################################################################")
    print("Ihre Website wird nun über diese Adresse: " + ip_addresse + ":5000 können sie die Website über einen Browser erreichen.")
    print("Dazu müssen sie Zeile 21 des ESP32 Codes mit folgendem austauschen:  MqttKommunikation mqtt('" + ip_addresse + "');")
    print("Sowie in Zeile 20 ihr WiFi Name sowie Passwort dessen eingeben. Bsp: WiFiVerbinde wifi('Ihr_WiFi_Name','Ihr_WiFi_Passwort');")
    print("################################################################################################################################")

    ServerStart()
