#include <Arduino.h>

//Einbindung der selbst geschriebenen Klassen 
#include "WiFiVerbindung.h"
#include "MqttVerbindung.h"
#include "GPIO_Ansteuerung.h"
#include "ZeitHandler.h"
#include "Adafruit_VEML7700.h"

#define led_tueroeffner_gruen 2
#define led_tueroeffner_rot 12
#define led_tuerkontakt_gruen 4
#define led_tuerkontakt_rot 13
#define led_wegbeleuchtung_gruen 19
#define led_wegbeleuchtung_rot 25


//Instanzierung der Objekte alle selbst geschriebenen Klassen 
//Übergeben werden SSID und Passwort des Netwzerks
WiFiVerbinde wifi("HaasHomeNetRep","67808585342282283998"); //Instanz der selbst geschriebenen Klasse WiFiVerbindung ers
MqttKommunikation mqtt("192.168.178.161");
String MqttKommunikation::thema_erhalten; 
GPIOKommunikation gpio; 

Adafruit_VEML7700 veml = Adafruit_VEML7700();
ZeitMessung LuxTimer;
ZeitMessung BeleuchtungTimer;
ZeitMessung TuerOeffnerTimer;

boolean tueroeffner_mqtt = false; 
boolean beleuchtung_mqtt = false; 

int reed_status = false; 


String thema; 
String nachricht; 


boolean klingeltaster = false; //Klingeltaster ist zu Beginn nicht gedrückt 
boolean klingeltaster_gedrueckt = false; 




void setup() {

  Serial.begin(115200);

  while(!veml.begin()) {
    delay(250);
    Serial.println("Lichtsenosor nicht gefunden. Versuche es erneut.");
  }

  wifi.VerbindungAufbauen(); //Versucht eine Verbindung zum WiFi-Netzwerk aufzubauen

  if(!mqtt.PruefeVerbindung()) //Wenn Verbindung zu MQTT-Broker unterbrochen ist, wird versucht neu zu verbinden 
  {
    mqtt.reconnect(); 
  }

  mqtt.Sende("ESP32Verbunden",gpio.LeseTuerkontaktKlingeltaster());
  
}

void loop() {

  if(!mqtt.PruefeVerbindung()) //Wenn Verbindung zu MQTT-Broker unterbrochen ist, wird versucht neu zu verbinden 
  {
    mqtt.reconnect(); 
  }

  //Falls die WiFi-Verbindung unterbrochen ist, wird ein reconnect versucht 
  if(!wifi.PruefeVerbindung())
  {
    wifi.VerbindungAufbauen();
  }

   //Dadurch wird dauerhaft eine Verbindung aufrecht erhalten und es wird ständig nach eintreffenden Nachrichten geschau
  mqtt.Aktualisiere(); 
  
  if(gpio.PruefeKlingelButtonZustand()==0) 
  {
    mqtt.Sende("Klingeltaster","Klingeltaster gedrueckt");
  }
  else if (gpio.PruefeKlingelButtonZustand()==1)
  {
    mqtt.Sende("Klingeltaster","Klingeltaster losgelassen");
  }


  reed_status = gpio.PruefeReedKontaktZustand();
  if (reed_status==1)
  {
    Serial.println("Tuer geoeffnet"); 
    gpio.AktiviereAusgang(led_tuerkontakt_rot); 
    gpio.DeaktiviereAusgang(led_tuerkontakt_gruen); 
    mqtt.Sende("Türkontakt", "Tuer offen"); 
  }
  else if(reed_status==0)
  {
    Serial.println("Tuer geschlossen"); 
    gpio.AktiviereAusgang(led_tuerkontakt_gruen); 
    gpio.DeaktiviereAusgang(led_tuerkontakt_rot); 
    mqtt.Sende("Türkontakt", "Tuer geschlossen");
    
  }
  

  
  if(LuxTimer.ZeitAbgelaufen(1000)){
    //Sorgt dafür, dass ZeitMesung gestartet wird
    LuxTimer.SetzeZustand(true);
    mqtt.Sende("Lichtstärke", String(veml.readLux()));
  }

  //Serial.println(MqttKommunikation::thema_erhalten); 
  //Empfängt MQTT Nachricht 

  
  if(mqtt.LetztesThema() == "Tueroeffner"){
    // Hier code für Öffnungstaster
    Serial.println("TUEROEFFNER TOPIC"); 
    //GPIO Türöffner aktivieren -> Tür wird geöffnet        Timer-Variable Türöffner = TRUE (5 Sekunden)     
    gpio.AktiviereAusgang(led_tueroeffner_gruen);  
    gpio.DeaktiviereAusgang(led_tueroeffner_rot); 

     //MQTT-Nachricht Türöffner HIGH (Rückmeldung)
    mqtt.Sende("TueroeffnerAntwort","Tueroeffner an"); 
    
    //Startet den Timer, damit Tueroeffner nach gewisser Zeit wieder deaktiviert wird 
    TuerOeffnerTimer.SetzeZustand(true);  
    tueroeffner_mqtt = false; 

    //GPIO Wegbeleuchtung aktivieren -> Wegbeleuchtung an   Timer-Variable Wegbeleuchtung = TRUE (10 Sekunden) 
    gpio.AktiviereAusgang(led_wegbeleuchtung_gruen); 
    gpio.DeaktiviereAusgang(led_wegbeleuchtung_rot); 
    
    //MQTT-Nachricht Wegbeleuchtung HIGH 
    mqtt.Sende("Wegbeleuchtung","Licht an"); 

    //Startet den Timer, damit Wegbeleuchtung nach gewisser Zeit wieder deaktiviert wird 
    BeleuchtungTimer.SetzeZustand(true);
    beleuchtung_mqtt = false;  
    
    
  }
  else if (mqtt.LetztesThema() == "ServerStart")
  {
    mqtt.Sende("ESP32Verbunden",gpio.LeseTuerkontaktKlingeltaster());
  }

  //Weiteres Else If (anderes Topic für digitalen Türöffner) 
  //GPIO Türöffner aktivieren -> Tür wird geöffnet        Timer-Variable Türöffner = TRUE (5 Sekunden)          
  //GPIO Wegbeleuchtung aktivieren -> Wegbeleuchtung an   Timer-Variable Wegbeleuchtung = TRUE (10 Sekunden) 
  //MQTT-Nachricht Wegbeleuchtung HIGH 
  




   if(TuerOeffnerTimer.ZeitAbgelaufen(5000) && beleuchtung_mqtt == false)
   {
    gpio.AktiviereAusgang(led_tueroeffner_rot);  
    gpio.DeaktiviereAusgang(led_tueroeffner_gruen); 

    mqtt.Sende("Wegbeleuchtung","Licht aus"); 

    beleuchtung_mqtt = true; 
   }


   if(BeleuchtungTimer.ZeitAbgelaufen(5000) && tueroeffner_mqtt == false)
   {
    gpio.AktiviereAusgang(led_wegbeleuchtung_rot); 
    gpio.DeaktiviereAusgang(led_wegbeleuchtung_gruen); 

    mqtt.Sende("TueroeffnerAntwort","Tueroeffner aus"); 

    tueroeffner_mqtt = true; 
   }
 

  //Weiteres Else If (anderes Topic für digitalen Türöffner) 
  //GPIO Türöffner aktivieren -> Tür wird geöffnet        Timer-Variable Türöffner = TRUE (5 Sekunden)          
  //GPIO Wegbeleuchtung aktivieren -> Wegbeleuchtung an   Timer-Variable Wegbeleuchtung = TRUE (10 Sekunden) 
  //MQTT-Nachricht Wegbeleuchtung HIGH 
  






  /*
  if(thema == "Tueroeffner")
  {
    if(nachricht_temp == "tuer_auf")
    {
      digitalWrite(led_tueroeffner, HIGH); 
    }
    else if(nachricht_temp = "tuer_zu")
    {
      digitalWrite(led_tueroeffner, LOW); 
    }
  }
  */
/*
  //Ist nur zum testen der LEDs 
  if(klingeltaster == LOW && klingeltaster_gedrueckt == false)
  {
   klingeltaster_gedrueckt=true; //Sorgt dafür, dass bei gedrücktem Taster nicht dauernd MQTT-Nachrichten gesendet werde
   client.publish("Klingeltaster","test");
   digitalWrite(led_wegbeleuchtung, HIGH); 
   Serial.println("Klingeltaster gedrückt"); 
  }
  else if(klingeltaster == HIGH && klingeltaster_gedrueckt==true)
  {
    klingeltaster_gedrueckt=false;
    client.publish("Klingeltaster","test");
    digitalWrite(led_wegbeleuchtung, LOW);
    Serial.println("Klingeltaster losgelassen");   
  }
  */

}
