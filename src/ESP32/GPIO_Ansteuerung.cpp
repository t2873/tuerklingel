// Wird benötigt um GPIO-Pins anzusteuern 
#include <Arduino.h>
// ButtonEvent.h ist abhängig von Bounce2.h
#include <Bounce2.h>
// Wird benötigt um OnClick-Events zu erkennen 
#include <ButtonEvents.h>
#include "GPIO_Ansteuerung.h"
// Erzeugt eine Instanz des Button Event Listeners für den Klingelbutton
ButtonEvents klingelbutton;

// Erzeugt noch eine Instanz aber für den Reed-Kontakt Sensor, den wir wie einen Button behandeln
ButtonEvents reedsensor;
  

GPIOKommunikation::GPIOKommunikation()
{
 
  // Definiert welche GPIO Pins Eingänge und welche GPIO Pins Ausgänge sind 
  pinMode(button_klingeltaster, INPUT_PULLDOWN); //Klingeltaster Eingang 
  pinMode(kontakt_reed, INPUT_PULLDOWN); //Sensor Eingang für Türsensor 



  pinMode(led_tueroeffner_gruen, OUTPUT); 
  pinMode(led_tueroeffner_rot, OUTPUT); 
  pinMode(led_tuerkontakt_gruen, OUTPUT); 
  pinMode(led_tuerkontakt_rot, OUTPUT);
  pinMode(led_wegbeleuchtung_gruen, OUTPUT);
  pinMode(led_wegbeleuchtung_rot, OUTPUT);

  // Man geht davon aus, dass Tuer zu beginn offen ist. Falls sie geschlossen ist, wird das im Loop erkannt und sofort automatisch auf Grün geschaltet
  digitalWrite(led_tuerkontakt_gruen, LOW); 
  digitalWrite(led_tuerkontakt_rot, HIGH); 
  
 
  


  // Fügt dem Button Eventlister den Klingeltaster zu 
  klingelbutton.attach(button_klingeltaster);
  //Da wir einen Pull-Down Widerstand haben und der Pin bei Knopfdruck high wird, stellen wie auf "active wenn High" 
  klingelbutton.activeHigh(); 
  //Stellt Debounce Zeit auf 15ms. In diesem Zeitraum wird kein Input registriert 
  klingelbutton.debounceTime(15); 


  reedsensor.attach(kontakt_reed); 
  reedsensor.activeHigh(); 
  reedsensor.debounceTime(15); 

}


void GPIOKommunikation::AktiviereAusgang(int pin)
{
  digitalWrite(pin, HIGH);  
}

void GPIOKommunikation::DeaktiviereAusgang(int pin)
{
  digitalWrite(pin, LOW);  
}


int GPIOKommunikation::PruefeKlingelButtonZustand()
{ 
  // Falls eine Änderung am Button vorliegt 
  if (klingelbutton.update() == true) {

    switch(klingelbutton.event()) {
      
      case (pressed) : {
        Serial.println("Klingeltaster wurde gedrückt");   
        return 0;        
        break;
      }

      // Soll denselben Effekt haben wie einfaches Drücken
      case (hold) : {
        Serial.println("Klingeltaster wird gehalten");
        return 0; 
        break;
      }

      default: 
      {
        return 2;       
        break; 
      }
    }
  }
}

int GPIOKommunikation::PruefeReedKontaktZustand()
{ 
  reedsensor.update(); 
  // Falls eine Änderung am Button vorliegt 
  
 if(reedsensor.fell() == true) 
  {
    Serial.println("Tür geöffnet"); 
    return 1; 
  }
  else if(reedsensor.rose() == true)
  {
    Serial.println("Tür geschlossen"); 
    return 0; 
  }
  else
  {
    return 2;
  }

}



// Methode ist nur dazu da, um zu Begin Werte einzulesen 
String GPIOKommunikation::LeseTuerkontaktKlingeltaster(){
  String tuerkontakt;
  String klingeltaster;
  if(digitalRead(kontakt_reed) == 1){
    tuerkontakt = "high";
  }
  else{
    tuerkontakt = "low";
  }
  if(digitalRead(button_klingeltaster) == 1){
    klingeltaster = "high";
  }
  else{
    klingeltaster = "low";
  }
  return tuerkontakt + "," + klingeltaster;

}
