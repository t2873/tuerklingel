#include "MqttVerbindung.h"

WiFiClient espClient; //Erstellt einen Client, welcher sich mit einer IP-Adresse verbinden kann 
PubSubClient client(espClient); 

MqttKommunikation::MqttKommunikation(String mqtt_broker_id)
{
  mqtt_broker = mqtt_broker_id; 

  client.setCallback(callback); //Checkt ob neue Nachrichten gekommen sind  
  client.setServer(mqtt_broker.c_str(), 1883); //Verbindet sich mit dem mqtt_broker über Port 1883 
  

  

  
}


//Empfängt eingehende MQTT-Nachrichten und gibt zwei Strings zurück
//1. String: Topic
//2. String: Nachricht 
void MqttKommunikation::callback(char* thema, byte* nachricht, unsigned int laenge)
{    
  Serial.print("Nachricht unter dem Thema: ");
  Serial.print(thema); 
  Serial.print("     Nachricht: "); 
 
  
  /*
  for (int i=0; i<laenge; i++)
  {
    Serial.print((char)nachricht[i]); 
    this->nachricht_temp += (char)nachricht[i]; 
  }
  Serial.println();
  */
  
  
  MqttKommunikation::thema_erhalten = thema;
  
}


bool MqttKommunikation::PruefeVerbindung()
{
  bool verbindung = false; 

  if(!client.connected())
  {
    verbindung = false; 
  }
  else
  {
    verbindung = true; 
  }

  return verbindung; 
}


void MqttKommunikation::reconnect()
{
  //Loopt solange, bis eine Verbindung steht 
  while(!PruefeVerbindung())
  {
    Serial.print("Versuche Verbindungsaufbau zu MQTT-Broker"); 

    if(client.connect("ESP32_Client_Tuerklingel")) //Wenn Verbindung besteht wird true zurückgegeben. "ESP32_Client_Tuerklingel" ist die einzigartige Client-ID des ESP32
    {
      client.subscribe("ServerStart");
      client.subscribe("WebsiteTuerOeffner");
      client.subscribe("Tueroeffner"); 
       
      Serial.println("Mit MQTT-Broker verbunden"); 
     
    }
    else
    {
      Serial.print("Verbindungsaufbau fehlgeschlagen, Fehlercode= "); 
      Serial.println(client.state()); 
      Serial.println("Erneuter Verbindugsversuch in 5 Sekunden"); 
      delay(5000); 
    }
  }
}

//Checkt ob es was neues gibt. Wenn ja, dann wird die Callback-Funktion aufgerufen 
void MqttKommunikation::Aktualisiere()
{
  client.loop(); 
}

void MqttKommunikation::Sende(String thema, String nachricht)
{
  client.publish(thema.c_str(),nachricht.c_str()); 
  Serial.print("Nachricht mit dem Thema: "); 
  Serial.print(thema); 
  Serial.print(" und der Nachricht: ");
  Serial.print(nachricht); 
  Serial.println(" wurde an MQTT-Broker gesendet"); 
}

String MqttKommunikation::LetztesThema(){
  String temp = this->thema_erhalten;
  this->thema_erhalten = "";
  return temp;
}
