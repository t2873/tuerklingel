#include "WiFiVerbindung.h"

WiFiVerbinde::WiFiVerbinde(String ssid, String passwort)
{
  Serial.println(ssid); 
  this->wifi_netzwerk_ssid = ssid; 
  this->wifi_passwort = passwort; 
  this->wifi_timeout = 2000; 
}

bool WiFiVerbinde::PruefeVerbindung()
{
  bool verbindung = false; 
  if(WiFi.status() != WL_CONNECTED)
  {
    verbindung = false; 
  }
  else
  {
    verbindung = true; 
  }
  return verbindung; 
}



void WiFiVerbinde::VerbindungAufbauen()
{
  Serial.print("Verbindungsversuch mit: "); 
  Serial.print(wifi_netzwerk_ssid); 

  WiFi.mode(WIFI_STA); //ESP32 ist die Sation, welche sich mit einem Access-Point (Handy) verbindet. Deswegen ist er im STA-Modus 
  WiFi.begin(wifi_netzwerk_ssid.c_str(),wifi_passwort.c_str()); //Verbindungsversuch zu WiFi wird gestartet

  this->verbindungsversuch_zeit = millis();

  while (!PruefeVerbindung()) //Solange nicht verbunden 
  {
    Serial.print("."); 
    delay (1000);
    if (millis() - verbindungsversuch_zeit > wifi_timeout)
    {
      Serial.println("Timeout: Verbindung fehlgeschlagen"); 
      Serial.println("Erneuter Verbindungsverusch"); 
      verbindungsversuch_zeit = millis(); //Zeit wird erneut festgelegt, damit neuer Timeout bestimmt werden kann 
    }
  }
  
  Serial.print("Verbindungsversuch zu: ");
  Serial.print(wifi_netzwerk_ssid); 
  Serial.println(" erfolgreich!"); 
  Serial.print("ESP Board MAC Adresse: ");
  Serial.println(WiFi.macAddress());
}
