#ifndef GPIO_ANSTEUERUNG 
#define GPIO_ANSTEUERUNG 

//GPIO-Pins werden intutive Namen gegeben 
#define button_klingeltaster 23
#define kontakt_reed 15

#define led_tueroeffner_gruen 2
#define led_tueroeffner_rot 12
#define led_tuerkontakt_gruen 4
#define led_tuerkontakt_rot 13
#define led_wegbeleuchtung_gruen 19
#define led_wegbeleuchtung_rot 25


class GPIOKommunikation{


  private: 

  // Hier wird der digitalRead vom Klingeltaster-Pin gespeichert 
  bool klingeltaster = false; 
  // Wird verwendet, um einen gedrückten Klingeltaster zu erkennen (mehrfaches Senden von MQTT Nachrichten wird unterbunden) 
  bool klingeltaster_gedrueckt = false; 

  public: 
  
  GPIOKommunikation();

  void AktiviereAusgang(int pin);
  void DeaktiviereAusgang(int pin); 
  
  void AktiviereLEDTueroeffner(); 
  void AktiviereLEDWegbeleuchtung(); 
  void AktiviereLEDTuerkontakt(); 

  // Speziell für den Klingeltaster 
  int PruefeKlingelButtonZustand();
  // Speziell für den Reed-Sensor 
  int PruefeReedKontaktZustand();

  String LeseTuerkontaktKlingeltaster();
  

  
  
};






#endif 
