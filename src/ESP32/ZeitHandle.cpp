#include "ZeitHandler.h"


ZeitMessung::ZeitMessung()
{
  initial_zeit_abgerufen = false;
}



bool ZeitMessung::ZeitAbgelaufen(unsigned long dauer)
{

  //Serial.println("Methode ZeitAbgelaufen ausgeführt"); 
  if(zeit_abgelaufen == false && initial_zeit_abgerufen == false)
  {
    // Holt sich den aktuellen TimeStamp 
    zeit_beginn = millis(); 
    initial_zeit_abgerufen = true; 
    //Serial.println("Initial Zeit geholt"); 
  }
  else if(zeit_abgelaufen == false && initial_zeit_abgerufen == true)
  {
    zeit_jetzt = millis(); 
    zeit_differenz = zeit_jetzt - zeit_beginn;

    // Overflow von Millis wird abgefangen und zeit_differenz korrekt berechnet
    if(zeit_jetzt < zeit_beginn){
      zeit_differenz = zeit_jetzt + (4,294,967,295 - zeit_beginn);
    }
     
    //Serial.print("Zeit Differenz: "); 
    //Serial.print(zeit_differenz); 
    //Serial.println(""); 
    if(zeit_differenz >= dauer)
    {
      zeit_abgelaufen = true; 
      initial_zeit_abgerufen = false; 
    }
    else
    {
      zeit_abgelaufen = false; 
    }
  }
  else
  {
    zeit_abgelaufen = true; 
  }

  //Serial.print("Zeit abgelaufen: "); 
  //Serial.print(zeit_abgelaufen); 
  //Serial.println(""); 

  return zeit_abgelaufen;
}



//Mit dieser Methode kann man von außen den aktuellen Zustand setzen 
//Bei einem Klingelevent setzt man klingel_event = true, wodurch zeit_abgelaufen = false wird -> Timer wird gestartet 
void ZeitMessung::SetzeZustand(bool klingel_event)
{
  if (klingel_event == true) 
  {
    zeit_abgelaufen = false; 
  }
}
