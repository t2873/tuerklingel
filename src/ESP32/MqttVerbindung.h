#ifndef MQTTVERBINDUNG_H
#define MQTTVERBINDUNG_H

#include <PubSubClient.h> // Wird benötigt, um MQTT als Client zu verwenden 
#include <WiFiClient.h>


class MqttKommunikation {

  private: 

  //Adresse des MQTT-Brokers 
  String mqtt_broker; 

  
  
  
  

  

  public: 

  static String thema_erhalten; 
  
  MqttKommunikation(String mqtt_broker_id); 

 
  
  static void callback(char* thema, byte* nachricht, unsigned int laenge); 
  void reconnect();

  bool PruefeVerbindung(); 

  void Aktualisiere(); 

  void Sende(String thema, String nachricht); 

  String LetztesThema();

  
};

#endif 
