#ifndef ZEITHANDLER_H 
#define ZEITHANDLER_H 

#include <Arduino.h>


class ZeitMessung
{

  private: 

  //Hier wird die Zeit zu Beginn der Zeitmessung gespeichert 
  unsigned long zeit_beginn = 0; 

  unsigned long zeit_jetzt = 0; 

  bool zeit_abgelaufen = true; 

  bool initial_zeit_abgerufen = false;  

  unsigned long zeit_differenz = 0; 


  public: 

  ZeitMessung(); 
  
  bool ZeitAbgelaufen(unsigned long dauer); 

  void SetzeZustand(bool klingel_event); 
}; 





#endif 
