#ifndef WIFIVERBINDUNG_H
#define WIFIVERBINDUNG_H

#include <WiFi.h> // Wird benötigt, um WiFi-Verbindung aufzubauen
#include <String.h>

// Erstellt einen Client der sich zu einer bestimmten IP-Adresse verbindet 


class WiFiVerbinde {
  
  private: 

   String wifi_netzwerk_ssid;
   String wifi_passwort;
   int wifi_timeout; //Timeout bei einem Verbindungsversuch 

   unsigned long verbindungsversuch_zeit; //Speichert die Zeit, zu welcher der Verbindungsversuch gestartet wurde 


  public: 

  //Konstruktor wird ssid und password übergeben 
  WiFiVerbinde(String ssid, String passwort); 

  //Macht initiale Verbindung mit WiFi
  void VerbindungAufbauen();

  bool PruefeVerbindung(); 


 
  
  
};


#endif 
