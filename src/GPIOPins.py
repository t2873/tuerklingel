import RPi.GPIO as gpio
from Singleton import Singleton

class GPIOPins(metaclass = Singleton):
    
    def __init__(self):
        
        # GPIO.BCM definiert welche von den zwei Pindefintionen verwendet wird 
        gpio.setmode(gpio.BCM) 
        tueroeffner_gpio = 4
        gpio.setup(tueroeffner_gpio, gpio.IN, pull_up_down=gpio.PUD_DOWN)
        
        
        gpio.add_event_detect(tueroeffner_gpio, gpio.BOTH, callback=mockTuerOeffnenSteigendeFallendeFlanke, bouncetime=15)
        
        
        


def mockTuerOeffnenSteigendeFallendeFlanke(gpiopin):
    import Funktionen
    if gpio.input(4):
        
        Funktionen.TuerOeffnenSteigendeFlanke(gpiopin)    
    else:
        Funktionen.TuerOeffnenFallendeFlanke(gpiopin)
        
    


        
        
        
        
       
       

# GPIO.add_event_detect(channel, GPIO.RISING, callback=my_callback)