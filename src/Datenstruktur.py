# Muss importiert werden, damit die Metaklasse Singleton verwendet werden kann 
# Wird auch bei allen anderen Klassen so gehandhabt 
from Singleton import Singleton
# Wird importiert um aktuelle Zeit zu bekommen
from datetime import datetime

# Klasse beinhaltet ein Wörterbuch, in welchem die aktuellen Zustände von Webseite und GPIO-Pins abgespeichert sind
# Kann das lokale Wörterbuch mit eingehenden Werten der Webseite updaten
# Gibt Wörtbücher für Datenbankzugriffe auf Tabellen "Log" und "Konfiguration" aus 
class Datenstruktur(metaclass = Singleton):
    
    # Erzeugt ein Python Dictionary (Wörterbuch). Speichert GPIO Zustände, letzte Besucher, Systemzustand und ausgewählter Begrüßungston
    Konfiguration = {

        # Liste der Zeitpunkte der letzten 5 Besucher
        "letzte_5_besucher":{
            "besucher1":{
                "uhrzeit":"",
                "datum":""
                }, # Hier muss 
            "besucher2":{
                "uhrzeit":"",
                "datum":""
                }, 
            "besucher3":{
                "uhrzeit":"",
                "datum":""
                }, 
            "besucher4":{
                "uhrzeit":"",
                "datum":""
                }, 
            "besucher5":{
                "uhrzeit":"",
                "datum":""
                }, 
            },
        
        # Zustände der Ein- und Ausgänge
        "gpio_zustaende" : {
            "tuerkontakt":"leer", #High wenn Tür geschlossen
            "tueroeffner":"leer", #High wenn Tür verriegelt ist  
            "klingeltaster":"leer", #High wenn jemand klingelt
            "oeffnungstaster":"low", #High wenn jemand den physischen Tür öffner drückt
            "wegbeleuchtung":"leer" #High wenn der Gehweg beleuchtet ist
        },
        
        # Encoded aufgenommenes Bild
        "base64_bild":"",
        
        # Aktueller Systemzustand (Automatisch, Manuel, Aus)
        "systemzustand":"",
        
        # Vom Benutzer ausgewählter begruessungston (Zufall, 1, 2, 3, ...)
        "begruessungston":"",
        
        "emails":[]
    }
    
    
    # Erzeugt Objekt und aktualisiert dabei direkt das oben erstellte Wörterbuch 
    def __init__(self, daten={}):
        self.AktualisiereDaten(daten)
        
    # Aktualliesirt Klassenattribute mit den werten des übergeben Dictionaries    
    def AktualisiereDaten(self, daten={}):
        self.Konfiguration.update(daten)
            
    # Gibt Dicitionary zurück, welches alle nötigen Werte für den Datenbank-Logeintrag enthält
    # Dazu werden die internen und geupdaten Daten aus dem Wörtberuch "Konfiguration verwendet"
    def LogEintragWerte(self):
        werte = {}
       
        werte["uhrzeit"] = datetime.now().strftime('%H:%M:%S')
        werte["datum"] = datetime.now().strftime('%d.%m.%Y')
        
        # Fügt dem Dictionary aktuelle Werte von GPIO Zustände und Systemzustand hinzu
        werte.update(self.Konfiguration["gpio_zustaende"]) 
        werte.update({"systemzustand":self.Konfiguration["systemzustand"]})

        
        
        # Für die Konfiguration beim Start des Webservers brauchen wir den letzten Stand des Begrüßungstons und des Systemzustands
        # Braucht man eig. nicht bei Log. Weil Methode unten aber gleich ist, führen wir es hier schon ein
        werte.update({"begruessungston":self.Konfiguration["begruessungston"]})
        
        return werte
      
    # Hier Konfigurationseintrag ruft Logeintrag auf, weil Methoden identisch sind
    def KonfiguartionsEintragWerte(self):
        return self.LogEintragWerte
    
    def EmailEmpfängerLöschen(self, Email):
        # Übergeben Email wird entfernt
        self.Konfiguration["emails"].remove(Email)

    
    def EmailEmpfängerHinzufügen(self,Email):
        # Übergeben Email wird an der vordersten stelle hinzugefügt
        self.Konfiguration["emails"].insert(0,Email)
        
    def Base64_Bild_erneuern(self,Base64_Bild):
        self.Konfiguration["base64_bild"] = Base64_Bild

