class Singleton(type):

    # Die Liste speichert ganzen Objekte, welche bereits von der Metaklasse instanziert wurden
    _instances = {}

    """*args und **kwargs ermöglichen die Übergabe von mehreren Parametern an eine Methode. Dabei kann eine beliebige und variierende Anzahl
    von Parametern übergeben werden. Bei **kwargs kann man im Gegensatz zu *args die einzelnen Parameter mit keywords ansprechen

    call ist eine Methode der Metaklasse, deren Aufgabe es ist, Instanzen der Metaklasse aufrufbar zu machen 
    cls ist die Klasse welche hier definiert wird, bei welcher man also verhindern möchte, dass sie mehrfach instanziert wird 
    Jedes mal wenn man die Unterklasse aufruft > MyClass() wird es als Singleton.__call__(MyClass) aufgefasst"""
    
    # Prüft ob bereits ein Objekt der Klasse erstellt wurde. Erstellt ein neues Objekt falls nicht  
    def __call__(cls, *argumente, **kwargs):
        
        # Wenn noch kein Objekt der Klasse instanziert wurde
        if cls not in cls._instances:
            
            # Wenn noch kein Objekt der Klasse in Liste ist, wird ein neuer Eintrag in Liste gemacht
            # Zudem wird ein Objekt der Klasse instanziert 
            cls._instances[cls] = super(Singleton, cls).__call__(*argumente, **kwargs)

        #Gibt das eben instanzierte Objekt zurück 
        return Singleton._instances[cls]

    # Löscht bereits instanzierte Objekte 
    def clear(cls):
        try:
            del Singleton._instances[cls]
        except KeyError:
            pass 
