#PyMysql ermöglicht Anbdindung an Datenbank mit Python 
import pymysql 
from Singleton import Singleton

# Steuert den kompletten Datenbankzugriff
# Kann Einträge in Datenbank machen und Daten aus Datenbank lesen und zurückgeben 


## SQL Absicherung nach dieser Quelle!!
## https://realpython.com/prevent-python-sql-injection/


class DB_Zugriff(metaclass = Singleton):


   # Erstellt Objekt welches Datenbankverbindung erstellt
   def __init__(self,db_adresse="localhost",db_benutzer="root",db_passwort="123456",db="flaskapp"):

       #Parameter müssen mit Keywords gleichgesetzt werden, weil Methode "pymysql.connect" so viele mögliche Parameter hat
       self.verbindung = pymysql.connect(host=db_adresse,user=db_benutzer,password=db_passwort,database=db)
      
   # Macht einen Log-Eintrag in die Datenbank. Nötige Daten werden von Klasse "Datenstruktur" bereitgestellt  
   def LogEintrag(self, daten):
       
       # Zeiger kann über die Reihen des Ergebnisses wandern, um diese einzeln zu verarbeiten 
       
       zeiger = self.verbindung.cursor()
       # Eigentlicher SQL-Befehl, welcher später ausgeführt wird 
       sql ="INSERT INTO log (uhrzeit, datum, systemzustand, tuerkontakt, tueroeffner, klingeltaster, oeffnungstaster, wegbeleuchtung) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"
      
       # Legt fest, dass folgender Befehl mit gegebenen Parametern ausgeführt werden soll 
       zeiger.execute(sql, (daten["uhrzeit"], daten["datum"], daten['systemzustand'], daten['tuerkontakt'], daten['tueroeffner'],daten["klingeltaster"], daten['oeffnungstaster'], daten['wegbeleuchtung'], ))
       
       # Sendet alle ausstehenden Befehle an Datenbank
       self.verbindung.commit()
       
       #Schließt den am Anfang erstellten Zeiger wieder
       zeiger.close()
       
   # Gibt als Ergebnis den Inhalt der gesamten SQL-Tabelle "Log" zurück 
   def LogAusLesen(self, sql="SELECT * FROM log ORDER BY 1 DESC"):
       
       

       zeiger = self.verbindung.cursor()
       
       #Wählt alle Einträge aus Tabelle "Log" aus 
       zeiger.execute(sql)
       
       self.verbindung.commit()
       
       #Speichert das Ergebnis der SQL-Query in der Variable self.ergebnis 
       ergebnis = zeiger.fetchall()
       
       zeiger.close()
       
       return ergebnis

   # Funktioniert genauso wie "LogEintrag". Es werden nur weniger Daten übermittelt   
   def KofigurationEintrag(self, daten):
       
       zeiger = self.verbindung.cursor()
       
       sql ="INSERT INTO konfiguration (systemzustand, begruessungston) VALUES (%s,%s)"
       
       zeiger.execute(sql, (daten['systemzustand'], daten['begruessungston'], ))
      
       self.verbindung.commit()
       
       zeiger.close()

   # Liest Daten welche für Konfiguration des internen Wörtberbuchs "Konfiguration" in Klasse "Datenstruktur" aus 
   # Dabei wird auf Tabelle Konfiguration und Log zugegriffen. Zugriff auf Log wird benöitgt, um letzte 5 Besucher auszulsen  
   def KonfigurationLesen(self):

        zeiger = self.verbindung.cursor()

        #Wählt den letzten Eintrag aus der Tabelle aus. Man braucht immer nur den aktuellsten Zustand 
        sql ="SELECT * FROM konfiguration ORDER BY 1 DESC LIMIT 1"

        zeiger.execute(sql)

        self.verbindung.commit()

        #Speichert das Ergebnis der SQL-Query in der Variable self.ergebnis 
        ergebnis_konfiguration = zeiger.fetchall()

        zeiger.close()

        # Ab hier wird dann auf den Log zugegriffen, um die letzten Besucher auszulsen 
        # Dazu wird die LogAusLesen von oben verwendet 
        
        ergebnis_besucher = self.LogAusLesen("SELECT * FROM log WHERE klingeltaster='high' ORDER BY 1 DESC LIMIT 5")



        # Hier wird die der die beiden SQL Querry in zwei Pythondictionarys umgewandelt
        # Umwandlung 1
        konfiguration_json = {
            "systemzustand":"",
            "begruessungston":""
        }

        konfiguration_json["systemzustand"] = ergebnis_konfiguration[0][1]
        konfiguration_json["begruessungston"] = ergebnis_konfiguration[0][2]
        
        #Umwandlung 2

        besucher_json = {
            "letzte_5_besucher":{
                "besucher1":{
                    "uhrzeit":"",
                    "datum":""}, 
                
                "besucher2":{
                    "uhrzeit":"",
                    "datum":""}, 
                
                "besucher3":{
                    "uhrzeit":"",
                    "datum":""}, 
                
                "besucher4":{
                    "uhrzeit":"",
                    "datum":""}, 
                
                "besucher5":{
                    "uhrzeit":"",
                    "datum":""},
            }
        }

        i = 1
        for eintrag in ergebnis_besucher:
            
            
            besucher_json["letzte_5_besucher"]["besucher"+ str(i)]["uhrzeit"] = eintrag[1] # 1 wenn id in sql
            besucher_json["letzte_5_besucher"]["besucher"+ str(i)]["datum"] = eintrag[2] # 2 wenn id in sql
            i += 1

       


        # Ende der Umwandlung
        
        return konfiguration_json, besucher_json
    
   # Methode um den letzten Logeintrag in einen String zu verpacken 
   # Wird später an ein Objekt der Klasse "Email_Dienst" übergeben
   def LetzterLogEintrag(self):
       
       # Liest den letzten Eintrag aus dem Log aus
       datalist = self.LogAusLesen("SELECT * FROM log ORDER BY 1 DESC LIMIT 1")
       
       # Generiert einen String aus dem ausgelesenen Log für die E-Mail Nachricht    
       Nachricht = "Am " + datalist[-1][2]+ " um " + datalist[-1][1] + " Uhr wurde Ihre Türklingel betätigt.\nDie Türklingel befand sich zum diesem Zeitpunkt im Systemzustand: " + datalist[-1][3] +".\nIm Anhang finden sie ein Bild des Besuchers." 

       return Nachricht 
   
   #Funktion zum Auslesen aller E-Mails in der Datenbank
   def EmailsAuslesen(self):
        sql="SELECT * FROM emails ORDER BY 1 DESC"
        
        zeiger = self.verbindung.cursor()
        
        #Wählt alle Einträge aus Tabelle "emails" aus 
        zeiger.execute(sql)
        
        self.verbindung.commit()
        
        #Speichert das Ergebnis der SQL-Query in der Variable self.ergebnis 
        emailliste = zeiger.fetchall()
        
        zeiger.close()
        
        emailarray = []
        
        # Email liste wird in ein Array umgewandelt
        for email in emailliste:
            emailarray.append(email[1])
            
        
        return {"emails":emailarray}

   
   # Funktion zum Hinzufügen der eingegebene E-Mail aus dem Frontend in die Datenbank
   def EmailEmpfängerHinzufügen(self,Email):
       
       zeiger = self.verbindung.cursor()
       
       sql ="INSERT INTO emails (email) VALUES (%s)"
       
       zeiger.execute(sql, (Email, ))
      
       self.verbindung.commit()
       
       zeiger.close()
   

    # Funktion zum Löschen der ausgewählten E-Mail im Frontend aus der Datenbank
   def EmailEmpfängerLöschen(self, Email):
       
       zeiger = self.verbindung.cursor()
       
       sql ="DELETE FROM emails WHERE email=(%s)"
       
       zeiger.execute(sql, (Email, ))
      
       self.verbindung.commit()
       
       zeiger.close()

      






    