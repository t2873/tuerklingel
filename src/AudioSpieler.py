# Klasse, welche für die Audioausgaben in der Weboberfläche verantwortlich ist
# die entsprechenden Audiodatein liegen im Repository ab. Auf diese wird über die URL zugegriffen. 
import os
os.environ['SDL_AUDIODRIVER'] = 'alsa'
from Singleton import Singleton
import pygame
import random

class AudioSpieler(metaclass = Singleton):
    
    def __init__(self):
        pass
    
    # Funktion für das Abspielen der Klingelsounds
    def SpieleKlingelTon(self):
        # Hier wird der Mixer von pygame initialisiert und der Klingelsound abgespielt
        
        pygame.mixer.init()
        pygame.mixer.music.load("static/Klingelsound/Klingelsound.mp3")
        pygame.mixer.music.play()

    # Funkion für das Abspielen der Begrüßungsansagen   
    def SpieleBegruessungsansage(self,audio_datei_name):
        if audio_datei_name == "zufall":
            audio_datei_name = "aufnahme" + str(random.randint(1,5))
        
        pygame.mixer.init()
        pygame.mixer.music.load("static/begruesungsaufnahmen/" + audio_datei_name +".mp3")
        pygame.mixer.music.play()
        