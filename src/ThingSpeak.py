from Singleton import Singleton
import urllib3

class ThingSpeakVerbindung(metaclass = Singleton):
    def __init__(self):
        
        
        # Defintion der verschiedenen API-Keys für das Uploaden der Werte in verschiedene "Fields" auf Thingspeak 
        self.url_werte = {
            "tuerkontakt" : "0",
            "tueroeffner" : "0", 
            "klingeltaster" : "0", 
            "oeffnungstaster" : "0", 
            "wegbeleuchtung" : "0"
        } 
        
    # Sendet sich aktuellen Werte der GPIO-Pins von ESP und Raspberry PI zur ThingSpeak-Cloud 
    def Aktualisiere_Werte(self, gpio_zustaende): 
       
        for wert in gpio_zustaende:
            # Ersetzt die GPIO Zustände high, low, leer in 1 und 0, damit Thingspeak damit arbeiten kann 
            if gpio_zustaende[wert] == "high":
                self.url_werte[wert] = "1"
            # Bei den Zuständen low und leer wird der Wert 0, weil Thingspeak nur binäre Darstellung ermöglicht 
            else:
                self.url_werte[wert] = "0"; 

        # Ereugt eine URL-Request mit allen Daten für verschiedenen GPIO-Pins an Thingspeak 
        self.fertige_url = "https://api.thingspeak.com/update?api_key=OQDWV1JPHQXVIHA0&field1="+self.url_werte["tuerkontakt"]+"&field2="+self.url_werte["tueroeffner"]+"&field3="+self.url_werte["klingeltaster"]+"&field4="+self.url_werte["oeffnungstaster"]+"&field5="+self.url_werte["wegbeleuchtung"]
        http = urllib3.PoolManager()
        r = http.request('GET', self.fertige_url)
        r.status
       
       
       
            
           
                
                
            
        
    